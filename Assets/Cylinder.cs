﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cylinder : MonoBehaviour {
	[Range(0,10)]
	public float cylinderSpeed;
	[Range(0,20)]
	public float attachedTransformSpeed;
	public Axis axis;
	public float length;														//Distance that the attatched object moves.
	public Transform attachedTransform;											//Attatched object that moves with the cylinder. Assigned through inspector.
	public Transform shaft;														

	//Inputs
	public bool advance {get;set;}
	public bool retract {get;set;}

	//Outputs
	public bool advanced {get;set;}
	public bool retracted {get;set;}

	private Vector3 _attatchedTransformAdvanced {
		get { return new Vector3(attachedTransform.position.x, attachedTransform.position.y + length, attachedTransform.position.z);}
	}
	private Vector3 _attatchedTransformRetracted {
		get { return new Vector3(attachedTransform.position.x, attachedTransform.position.y - length, attachedTransform.position.z);}
	}
	private float _advancedY = 1.0f;
	private float _retractedY = 0.05f;

	// Use this for initialization
	void Start () {
		shaft = transform.GetChild(0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public IEnumerator advanceCylinder() {
		if (advanced)
		{
			yield break;
		}
		retracted = false;
		Vector3 target = transform.position;
		Vector3 shaftAdvance = new Vector3(shaft.localPosition.x, _advancedY, shaft.localPosition.z);
		if (attachedTransform != null)
		{																			//assign the travel distance to the proper axis;
			switch (axis)														
			{
				
				case Axis.X:
					target = new Vector3 (attachedTransform.position.x + length, attachedTransform.position.y, attachedTransform.position.z);
					break;

				case Axis.Y:
					target = new Vector3 (attachedTransform.position.x, attachedTransform.position.y + length, attachedTransform.position.z);
					break;

				case Axis.Z:
					target = new Vector3 (attachedTransform.position.x, attachedTransform.position.y, attachedTransform.position.z + length);
					break;
			}
		}

		while (advance && Vector3.Distance(shaft.localPosition, shaftAdvance) > 0.01f)
		{
			shaft.localPosition = Vector3.MoveTowards(shaft.localPosition, shaftAdvance, Time.deltaTime * cylinderSpeed);
			if (attachedTransform != null)
			{
				attachedTransform.position = Vector3.MoveTowards(attachedTransform.localPosition, target, Time.deltaTime * attachedTransformSpeed);
			}
			yield return null;
		}
		if (attachedTransform != null)
		{
			while (Vector3.Distance(attachedTransform.position, target) > 0.01f)
			{
				attachedTransform.position = Vector3.MoveTowards(attachedTransform.localPosition, target, Time.deltaTime * attachedTransformSpeed);		
				yield return null;
			}
		}
		advanced = true;

		yield return null;	
	}

	public IEnumerator retractCylinder() 
	{
		if (retracted)
		{
			yield break;
		}
		advanced = false;
		Vector3 target = transform.position;
		Vector3 shaftRetract = new Vector3(shaft.localPosition.x, _retractedY, shaft.localPosition.z);
		if (attachedTransform != null)
		{
			switch (axis)
			{
				case Axis.X:
					target = new Vector3 (attachedTransform.position.x - length, attachedTransform.position.y, attachedTransform.position.z);
					break;

				case Axis.Y:
					target = new Vector3 (attachedTransform.position.x, attachedTransform.position.y - length, attachedTransform.position.z);
					break;

				case Axis.Z:
					target = new Vector3 (attachedTransform.position.x, attachedTransform.position.y, attachedTransform.position.z - length);
					break;
			}
		}

		while (retract && Vector3.Distance(shaft.localPosition, shaftRetract) > 0.01f)
		{
			Debug.Log("Retracting Cylinder");
			shaft.localPosition = Vector3.MoveTowards(shaft.localPosition, shaftRetract, Time.deltaTime * cylinderSpeed);
			if (attachedTransform != null)
			{
				attachedTransform.position = Vector3.MoveTowards(attachedTransform.localPosition, target, Time.deltaTime * attachedTransformSpeed);
			}
			yield return null;
		}
		if (attachedTransform != null)
		{
			while (Vector3.Distance(attachedTransform.position, target)>0.01f)
			{
				attachedTransform.position = Vector3.MoveTowards(attachedTransform.localPosition, target, Time.deltaTime * attachedTransformSpeed);		
				yield return null;
			}
		}
		retracted = true;
		yield return null;	
	}

	void OnGUI()
	{
		if (GUI.Button(new Rect(5,5,120,30), "Advance")) {
			StopAllCoroutines();
			retract = false;
			advance = true;
			StartCoroutine(advanceCylinder());
		}

		if (GUI.Button(new Rect(5,40,120,30), "Retract")) {
			StopAllCoroutines();
			advance = false;	
			retract = true;
			StartCoroutine(retractCylinder());
		}
	}
}
